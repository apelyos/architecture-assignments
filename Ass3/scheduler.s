        global scheduler
        extern resume, end_co


section .text

; gets t, K, length, width
scheduler:
        mov ebx, 1
.next:
        call resume             ; resume printer
        loop .next

        call end_co             ; stop co-routines