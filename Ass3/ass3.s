; ***************************************************************
; *				MACROS 				*
; ***************************************************************

; save numeric parameter, 1: offset, 2: addr to save
%macro HandleParam 2
    mov ebx, %1
    push eax
    push ebx
    call atoi
    pop ebx
    mov DWORD %2, eax 
    pop eax
%endmacro

%macro printInt 1
  push %1
  push LC3
  call printf
  add esp, 8
%endmacro

%macro printString 1
  push %1
  push LC4
  call printf
  add esp, 8
%endmacro


; ***************************************************************
; *			MAIN Program				*
; ***************************************************************

    global 	main, _start, array_element_address 
    global 	WorldWidth, WorldLength, PrintInter, Generations, state
    extern 	init_co, start_co, resume
    extern 	scheduler, printer, cell, atoi, printf, malloc


        ;; /usr/include/asm/unistd_32.h
sys_exit:       equ   1

section .bss
  WorldWidth: 	resb 4 ;integer
  WorldLength: 	resb 4 ;integer
  FileName:	resb 4 ;pointer
  PrintInter:	resb 4 ;integer (k)
  Generations:	resb 4 ;integer (t)
  state:	resb 4 ;pointer to array of the data
  fd_in:	resb 4 ;file descriptor
  buffer:	resb 1 ;read buffer

section .data

section	.rodata
LC0: DB	"Error: Wrong num of params.",10,"ass3 <filename> <length> <width> <t> <k>", 10, 0
LC3: DB	"%d", 10, 0				; Format string		
LC4: DB	"%s", 10, 0				; Format string	

section .text

;; args: 'ass3' <filename> <length> <width> <t> <k>
start:
main:
        enter 0, 0
        
        mov DWORD ebx, [esp+8] ; num of params
        cmp ebx, 6 	 ; check number of params = 6
        je paramsHandling
	push LC0
	call printf
	jmp exit
        
        paramsHandling:
	mov eax, [esp+12] ;  param array
	mov ebx, [eax+4]
	mov DWORD [FileName], ebx; save filename param
	
	HandleParam [eax+8] , [WorldLength]
	HandleParam [eax+12], [WorldWidth]
	HandleParam [eax+16], [Generations]
	HandleParam [eax+20], [PrintInter]
	
	; mem alloc of WorldLength*WorldWidth bytes (for array)
	mov eax, [WorldLength]
	mul DWORD [WorldWidth]	; WorldLength*WorldWidth
	push eax
	call malloc	
	mov DWORD [state], eax
	
	; load the data file into array
	call load_file
	
	jmp exit ;;;;; REMOVE THIS ;;;;

        xor ebx, ebx            ; scheduler is co-routine 0
        mov edx, scheduler
        mov ecx, [ebp + 4]      ; ecx = argc
        call init_co            ; initialize scheduler state

        inc ebx                 ; printer i co-routine 1
        mov edx, printer
        call init_co            ; initialize printer state
        
        ;;;;;;;; NEED TO initialize alot of cell co routines
        inc ebx                 ; printer i co-routine 2
        mov edx, cell
        call init_co            ; initialize printer state

        
        xor ebx, ebx            ; starting co-routine = scheduler
        call start_co           ; start co-routines


        exit:
        mov eax, sys_exit
        xor ebx, ebx
        int 80h

        
load_file:
    ;open the file for reading
    mov eax, 5
    mov ebx, [FileName]
    mov ecx, 0          ;for read only access
    mov edx, 0777       ;read, write and execute by all
    int  0x80
    mov  [fd_in], eax
    
;read 1 byte from file
    mov eax, 3
    mov ebx, [fd_in]
    mov ecx, buffer
    mov edx, 1
    int 0x80
    
    printString buffer
    
; close the file
    mov eax, 6
    mov ebx, [fd_in]    
    
    ret
        
; (Array_start, Element_size, Array_width, x, y)     
; return = addr+size*(width*y + x)
 array_element_address:   
    push ebp 
    mov ebp, esp
    
    mov eax, [ebp + 24] ; y
    mov ebx, [ebp + 16] ; width
    mul ebx
    add eax, [ebp + 20]
    mov ebx, [ebp + 12] ; size
    mul ebx
    add eax, [ebp + 8] ; addr

    mov esp, ebp
    pop ebp
    ret 