section .data                    	; data section, read-write
        an:    DD 0              	; this is a temporary var

section .text                    	; our code is always in the .text section
        global strToLeet          	; makes the function appear in global scope
        extern printf            	; tell linker that printf is defined elsewhere 				; (not used in the program)

strToLeet:                        	; functions are defined as labels
        push    ebp              	; save Base Pointer (bp) original value
        mov     ebp, esp         	; use base pointer to access stack contents
        pushad                   	; push all variables onto stack
        mov ecx, dword [ebp+8]	; get function argument

;;;;;;;;;;;;;;;; FUNCTION EFFECTIVE CODE STARTS HERE ;;;;;;;;;;;;;;;; 

	mov	dword [an], 0		; initialize answer
	
LoopChars:
   
  ; jump if ascii A:
  cmp 	byte [ecx], 65
  je	A
  
  ; jump if ascii B:
  cmp 	byte [ecx], 66
  je	B
  
  ; jump if ascii C:
  cmp 	byte [ecx], 67
  je	C
  
  ; jump if ascii E:
  cmp 	byte [ecx], 69
  je	E
  
  ; jump if ascii G:
  cmp 	byte [ecx], 71
  je	G
  
  ; jump if ascii H:
  cmp 	byte [ecx], 72
  je	H
  
  ; jump if ascii I:
  cmp 	byte [ecx], 73
  je	I
  
  ; jump if ascii L:
  cmp 	byte [ecx], 76
  je	L
  
  ; jump if ascii O:
  cmp 	byte [ecx], 79
  je	O
  
  ; jump if ascii S:
  cmp 	byte [ecx], 83
  je	S
  
  ; jump if ascii T:
  cmp 	byte [ecx], 84
  je	T
  
  ; jump if ascii Z:
  cmp 	byte [ecx], 90
  je	Z
  
  ; Check if we have any other big letter (ascii number between 65 and 90)
  cmp	byte [ecx], 65
  jl	NextIter
  cmp	byte [ecx], 90
  jg	NextIter
  add	byte [ecx], 32 ; add the ascii value to turn into small letters
  
  jmp NextIter ; jump to the next iteration
	
A:
  mov	byte [ecx], 52
  jmp EndOfCase

B:
  mov	byte [ecx], 56
  jmp EndOfCase
  
C:
  mov	byte [ecx], 40
  jmp EndOfCase
  
E:
  mov	byte [ecx], 51
  jmp EndOfCase
  
G:
  mov	byte [ecx], 54
  jmp EndOfCase
  
H:
  mov	byte [ecx], 35
  jmp EndOfCase
  
I:
  mov	byte [ecx], 33
  jmp EndOfCase
  
L:
  mov	byte [ecx], 49
  jmp EndOfCase
  
O:
  mov	byte [ecx], 48
  jmp EndOfCase
  
S:
  mov	byte [ecx], 53
  jmp EndOfCase
  
T:
  mov	byte [ecx], 55
  jmp EndOfCase
  
Z:
  mov	byte [ecx], 50
  jmp EndOfCase
  

EndOfCase:  
  inc byte [an] ;increment the counter of switches
  
NextIter:
	inc	ecx      		; increment pointer
	cmp	byte [ecx], 0    	; check if byte pointed to is zero
	jnz	LoopChars       	; keep looping until it is null terminated

;;;;;;;;;;;;;;;; FUNCTION EFFECTIVE CODE ENDS HERE ;;;;;;;;;;;;;;;; 
         popad                    ; restore all previously used registers
         mov     eax,[an]         ; return an (returned values are in eax)
         mov     esp, ebp
         pop     ebp
         ret
