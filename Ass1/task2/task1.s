section	.rodata
LC0:
	DB	"The result is: %s", 10, 0	; Format string
HEX_Map:	
	DB  '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'

section .bss
LC1:
	RESB	32

section .text
	align 16
	global my_func
	extern printf

my_func:
	push	ebp
	mov	ebp, esp	; Entry code - set up ebp and esp
	pusha			; Save registers

	mov ecx, dword [ebp+8]	; Get argument (pointer to string)
	
	; My code begin: ###################################

	mov esi, HEX_Map  	; Pointer to hex-character table
	mov edi, LC1 		; pointer to result memory
	
	mov 	bl, 1		; reset orig char counter
	
	count_chars:
	cmp 	byte [ecx], 10	; compare to '\n' char
	je	convert		; end of line, jump to convert
	inc	ecx 		; get next char
	inc	bl 		; count chars
	jmp 	count_chars
	
	convert:
	mov	dx, 1		; reset current bit counter
	mov	eax, 0		; reset current val
	
	convert_iter:
	dec	bl;		; count back chars
	jz store_last		; if it's 0 goto 'store'
	
	dec 	ecx;		; prev char (were iterating from the end)
	cmp 	byte [ecx], 48	; compare current char to '0' (else assuming 1)
	je	is_zero		; if zero skip next stage
	add	 ax,	dx	; add the current val
	is_zero:    	
	shl	dx, 1		; shiftleft the bit counter
	cmp dx, 16		; if we not over MSB of hex
	jne convert_iter	; jump to next iter
	jmp store		; else, store it
	
	store_last:
	cmp ax, 0		; if last char is 0, ignore it
	je finish_convert	; and jump to end
	
	store:
	mov esi, HEX_Map 	; compute address of hex char
	add esi, eax		; get the right ascii hex char according to the map
	mov al, byte [esi] 	; get hex ascii code
	mov byte [edi], al	; store hex val
	inc edi				; increase result char counter
	
	cmp bl, 0			; if we at the end of the string
	je finish_convert	; goto finish converting
	jmp convert			; else, keep converting
    	
	finish_convert:
	mov byte [edi], 0 	; add null char in the end
	
	reverse_result:
	mov eax, LC1		; eax points to the end of string
	mov esi, eax  		; esi points to start of string
	mov ecx, edi		; ecx will hold the length
	sub ecx, eax		; length = end - start
	dec edi       		; edi points to end of string
	shr ecx, 1    		; ecx is count (length/2)
	jz done       		; if string is 0 or 1 characters long, done
	reverseLoop:
	mov al, [esi] 		; load characters
	mov bl, [edi]
	mov [esi], bl 		; and swap
	mov [edi], al
	inc esi       		; adjust pointers
	dec edi
	dec ecx       		; and loop
	jnz reverseLoop
	
	done:
	
	; My code end ###################################
	push	LC1		; Call printf with 2 arguments: pointer to str
	push	LC0		; and pointer to format string.
	call	printf
	add 	esp, 8		; Clean up stack after call

	popa			; Restore registers
	mov	esp, ebp	; Function exit code
	pop	ebp
	ret

