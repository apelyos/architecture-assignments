 section	.rodata
LC0:
	DB	"x is out of range", 10, 0	; Format string
LC1:
	DB	"The answer is: %d", 10, 0	; Format string

	
section .bss
X:
	resb 32


section .text
	align 16
	global calc_func
	extern printf
	extern check

calc_func:
	push	ebp
	mov	ebp, esp	; Entry code - set up ebp and esp
	pusha			; Save registers
	
	mov ecx, dword [ebp+8]	; Get argument (pointer to int)
	mov	[X],ecx
	push 	ecx		;put argument
	call 	check		;call check 
	cmp 	eax, 0		;if check is 0 print error else calc and print
	je	print_error	
	mov	ebx, [X]	
	mov 	esi,1		;the initial number for sqr--->1
	

multiply:
	cmp	ebx, 0
	je	devision
	shl	esi,1		;move the '1' left --> the same as multiply in 2
	dec	ebx		;until we acomlish sqr(2,X)
	jmp	multiply

devision:
	mov  	edx,0
	mov 	eax,esi
	mov	ecx, [X]
	div 	ecx	 ;div is from the registers edx,eax. the ans will be in eax	
print_ans:
	push 	eax		;pushing arguments
	push 	LC1		
	call 	printf		;printing ans
	add 	esp, 16		;Clean up stack after call
	jmp	end


print_error:
	push	LC0		;pushing argument
	call	printf		; printing error
	add 	esp, 8		;Clean up stack after call
	

	
end:	
	

	popa			; Restore registers
	mov	esp, ebp	; Function exit code
	pop	ebp
	ret

