%macro check 2    ;first-->need to be,  second-->> will be add
  push %1	;how much there need to be in stack
  push %2	;how much will be add to the stack
  call checkstack
  add esp,8    ;;clean stack
%endmacro
%macro printChar 1
  push %1
  push LC2
  call printf
  add esp, 8
%endmacro
%macro printfirstChar 1
  push %1
  push LC8
  call printf
  add esp, 8
%endmacro
%macro printInt 1
  push %1
  push LC3
  call printf
  add esp, 8
%endmacro
%macro printString 1
  push %1
  push LC4
  call printf
  add esp, 8
%endmacro
%macro pushN 1
  push edx
  push ebx
  mov edx, %1
  mov ebx, [offest]
  mov [STACK+4*ebx], edx
  mov ebx ,[offest]
  inc ebx
  mov  [offest], ebx
  pop ebx
  pop edx
%endmacro 
%macro popN 1
  mov eax, dword [offest]
  sub eax, 1  ;;the last 
  mov %1, [STACK+4*eax]
  mov eax, dword [offest]
  sub eax, 1
  mov dword [offest],eax
%endmacro
%macro startFunction 0
  mov ebp, esp ; Entry code - set up ebp and esp
  pushad			; Save registers
%endmacro 
%macro endFunction 0
  popad
  mov esp,ebp
%endmacro
%macro endFunctionAns 0
  mov dword[ans], eax
  popad
  mov esp,ebp
  mov eax, dword[ans]
%endmacro
%macro createNode 0
  startFunction
  push 5		;;size of each node is 5 bytes---> 1 for number and 4 for next addrres 
  call malloc     ;create node
  add esp ,4	;clear stack
  endFunctionAns
%endmacro
%macro println 0   ;printf \n
  push LC5
  call printf
  add esp, 4 ;;clear stack
%endmacro
%macro case 3
  cmp	bl,%1
  jne	%3
  call 	%2
  jmp	start
%endmacro
%macro checkAndJump 3
  check %1,%2
  cmp  eax,1     ;if the check return 0 return to start
  je   %3
  ret
%endmacro
%macro movB 2   
  push eax
  mov al,  byte [%2]
  mov byte [%1], al
  pop eax
%endmacro
  
 SIZE EQU 5
   section	.data
offest:	DD	0
 
 section	.rodata
LC0:
	DB	"Error: Operand Stack Overflow", 10, 0	; Format string
LC1:
	DB	"Error: Insufficient Number of Arguments on Stack", 10, 0	; Format string
LC2:
	DB	"%02hhX", 0	; Format string (XXX)
LC3:
	DB	"%d", 10, 0	; Format string	
LC4:
	DB	"%s", 10, 0	; Format string	
LC5:
	DB	"", 10, 0	; Format string		
LC6:
	DB	"Error: exponent too large", 10, 0	; Format string		
LC7:
	DB	"calc: ", 0	; Format string		
LC8:
	DB	"%hhX", 0	; Format string (XXX)
HEX_Map:	
	DB  '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'
	
	
section .bss
BUFFER:
	resb 80
STACK:
	RESD SIZE 
ans:
	RESB    32
ans2:
	RESB    32
newList:
	RESB    32
oldList:
	RESB    32
	
section .text
     align 16
     global main
     extern printf
     extern malloc
     extern gets
main: 

  
  
  
start:
  push 	LC7
  call printf
  add  esp,4
  call  read    	;get new input to BUFFER
  mov	bl, byte [BUFFER]
  cmp	bl,'q'
  je	end		;if recived 'q'--> end program
divExpr:
  case '^', shl ,v
v:
  case 'v', shr ,pl
pl:
  case '+', plus,d
d:
  case 'd', dup,p
p:
  case 'p',pp,n
n:
  case 'n', number, nethier
nethier:
  check 0,1
  cmp  eax,1     ;if the check return 0 return to start
  je   nethier_c
  jmp start
  mov ebx,0
  cmp byte [BUFFER],0
  je  finishConvertion2      ;if enter nothing its like entering zero, so we push 0 to the stack
nethier_c:  
  mov ecx, 0		;;create counter
findEnd:
  cmp byte [ecx+BUFFER],0 ; XXX
  je foundEnd
  inc ecx 		;;inc couter
  jmp findEnd
foundEnd:
  cmp ecx,0
  je start
  cmp ecx, 1
  je checkIfIsZero
notZero:
  mov dword [newList],0
  cmp ecx, 0          ;;;if ecx==counter is 0 end conv
  je finishConvertion   
  createNode	;;first node
  mov ebx,eax
  mov dword[newList], eax
untilFinshConv:

  cmp ecx ,1          ;;if ecx==counter is 1 jumpr
  je onlyOneLeft
  sub ecx, 2  ;;ecx -->pointer to the last number that hasent been converted

  
createNumLoop:
  mov eax, 0
  mov ax ,word  [ecx+BUFFER]	 ;;put the 2 chars in ax
  mov dl, ah ; switch chars (XXX)
  mov ah, al
  mov al, dl
convetNumber: 
  sub ah, 48	;finxing asci to 1,2,3...	
  sub al, 48
  
  mov dh, 10
  cmp  dh,ah	;;if 10 is bigger than it wasn't  A,B,C...
  jg firstIsOk
  sub ah, 7    ;fixing a,b,c,d... to 10,11,12,..
firstIsOk:
  cmp byte dh,al	;;if 10 is bigger than it wasn't  A,B,C...
  jg secondIsOk
  sub al, 7
secondIsOk:
  
  shl ah, 4 
  add ah, al   ;;create the new number
  mov byte [ebx] ,ah ;put number in nude
  mov edx, 0
  mov dl,ah
  add ebx, 1      ;make it point to the pointer to the next nude
  cmp ecx, 0          ;;;if ecx==counter is 0 end conv
  je finishConvertion 
  createNode
  mov dword[ebx], eax
  mov ebx, eax
  jmp untilFinshConv


onlyOneLeft:
  mov al ,byte  [BUFFER]	 ;;put the 1 char in al
  sub al, 48
  mov dh, 10
  cmp  dh,al	;;if 10 is bigger than it wasn't  A,B,C...
  jg numberIsOK
  sub al, 7
numberIsOK:
  mov byte [ebx] ,al ;put number in nude
  add ebx,1

  
finishConvertion:
  mov dword [ebx],0 		;;init the pointer to 0 
  mov ebx, dword [newList]
finishConvertion2:
  pushN ebx
  jmp start
  
checkIfIsZero:
  mov ebx,0
  cmp byte [BUFFER],'0'
  je  finishConvertion2      ;if enter nothing its like entering zero, so we push 0 to the stack
  jmp notZero
  
end:
  mov eax, 1
  mov ebx, 0
  int	0x80
  
plus:
    checkAndJump 2,0,plus_c
plus_c:
   popN edx
   popN ebx
   cmp edx,0
   jne p_firstnotZero
   pushN ebx
   ret
p_firstnotZero:
   cmp ebx,0
   jne p_secondnotZero
   pushN edx
   ret
p_secondnotZero:   
   mov dword[newList],edx
   mov cl, byte[ebx]
   add byte [edx] ,cl
   jc  isCarry
   
notCarry:
   cmp dword [ebx+1],0
   je firstDoneNotCarry
   cmp dword [edx+1],0
   je secondDoneNotCarry
   mov ebx, [ebx +1]
   mov edx, [edx +1]
   mov cl, byte[ebx]
   add byte [edx] ,cl
   jc isCarry
   jmp notCarry
   
isCarry:
   cmp dword [ebx+1],0
   je firstDoneCarry
   cmp dword [edx+1],0
   je secondDoneCarry
   mov ebx, [ebx +1]
   mov edx, [edx +1]
   mov cl, byte[ebx]
   inc cl
   jc willBeCarry
   add byte [edx] ,cl
   jc isCarry
   jmp notCarry

willBeCarry:
    add byte [edx] ,cl
    jmp isCarry
    
firstDoneNotCarry:
    jmp endPlus
    
secondDoneNotCarry:
    mov ecx, dword [ebx+1]
    mov dword [edx+1] ,ecx
    jmp endPlus
firstDoneCarry:
    cmp dword [edx+1],0
    je addNodetoLastAdd1
    jmp add1
secondDoneCarry:
    mov ecx, dword [ebx+1]
    mov dword [edx+1] ,ecx
    jmp add1

add1:
    cmp dword[edx+1],0
    je addNodetoLastAdd1
    mov edx,[edx+1]
    inc byte [edx]
    cmp byte [edx],0
    je add1
    
endPlus:
    mov eax,dword[newList]
    pushN eax
    ret
    
addNodetoLastAdd1:
    createNode
    mov dword[edx+1],eax
    mov byte [eax],0
    mov dword [eax+1],0
    jmp add1
    
    
pp:
  checkAndJump 1,0,pp_c
pp_c:

  popN edx  ;edx contain now the last node &&&&&& offest= offest-1
  cmp edx,0
  je printZero
  push 0xffffffff   ;;push a delimiter to the stack
pushLoop:
  cmp edx,0   ;;check if end of list
  je printfirst
  mov eax,0
  mov  al, byte [edx]
  push eax   
  mov edx, dword [edx+1];;acsses next node
  jmp pushLoop
printfirst:
  pop eax
  cmp eax,0xffffffff      ;;if we arrive to delimiter, stop printing
  je endPrint
  printfirstChar eax


printLoop:
  pop eax
  cmp eax,0xffffffff      ;;if we arrive to delimiter, stop printing
  je endPrint
  printChar eax
  jmp printLoop

endPrint:
  println
  ret

printZero:
  mov eax,0
  printInt eax
  ret
  
  
dup:
  checkAndJump 1,1,dup_c
dup_c:
  startFunction
  popN edx
  mov ebx, 0
  cmp edx, 0
  je endDup
  ;;;first time
  createNode
  mov ebx, eax  ;ebx contains the first node now
  movB  ebx, edx     ;mov 1 byte of the memory cotent
  mov dword[newList],ebx
  mov dword[oldList],edx
  add ebx,1 
  add edx,1
dup_loop:
  mov eax, dword[edx]
  cmp dword[edx],0
  je retriveNode
  createNode
  mov [ebx], eax
  mov ebx, eax
  mov edx, [edx]
  movB ebx, edx
  add ebx,1 
  add edx,1
  jmp dup_loop
  
retriveNode:
  mov dword[ebx],0   ;make the last node to point nothing
  mov ebx,dword[newList]
  mov edx,dword[oldList]
  
endDup:
  mov eax, edx
  pushN eax
  mov eax, ebx
  pushN eax
  endFunction
  ret


  
  
  
  
  
  
  
  
  
shl:
  checkAndJump 2,0,shl_c
shl_c:
  popN edx    ; edx is x!!!
  popN ebx	;ebx is y!!!
  mov ecx,ebx
  push ecx
  call checkExponent
  add esp,4
  cmp eax,0
  je shlErrorEnd
  mov ecx,ebx
  push ecx
  call getNumber   ;now eax contain y
  add esp, 4
  mov ebx,7
  and ebx,eax   ;;ebx have sherit haluka
  and eax, 0xfffffff8  
  mov ecx, eax   ;ecx contain the number of node to throw
.loop:       ;;add a new node with value 0
  cmp ecx,0
  je addZero
  createNode
  mov byte[eax],0
  mov dword[eax+1],edx
  mov edx, eax
  sub ecx,8
  jmp .loop
addZero:
  mov ecx, edx
  push ecx
  call addZeroBeg
  add esp,4
shiftL:
  cmp ebx,0
  je endShl
  mov ecx, edx
  push ecx
  call shlFun 
  add esp,4
  dec ebx
  jmp shiftL

endShl:
  mov ecx, edx
  push ecx
  call cleanZero
  add esp, 4
  pushN edx
  ret
shlErrorEnd:
  ret
  
  

  
  
  
  
  
  
  
  


shr:
  checkAndJump 2,0,shr_c
shr_c:
  popN edx    ; edx is x!!!
  popN ebx	;ebx is y!!!
  mov ecx,ebx
  push ecx
  call checkExponent
  add esp,4
  cmp eax,0
  je shrErrorEnd
  mov ecx,ebx
  push ecx
  call getNumber   ;now eax contain y
  add esp, 4
  mov ebx,7
  and ebx,eax   ;;ebx have sherit haluka
  and eax, 0xfffffff8  ;eax contain the number of node to throw
.loop:       ;;delete the first node
  cmp eax,0
  je shiftR
  cmp edx, 0
  je endShr
  mov edx, [edx+1]
  sub eax,8
  jmp .loop
  
shiftR:
  cmp ebx,0
  je endShr
  mov ecx, edx
  push ecx
  call shrFun 
  add esp,4
  dec ebx
  jmp shiftR

endShr:
  mov ecx, edx
  push ecx
  call cleanZero
  add esp, 4
  pushN edx
  ret
shrErrorEnd:
  ret
  

number:
  checkAndJump 1,0,number_c
number_c:
  startFunction
  popN edx ;;edx contain the latest element
  mov ecx,0   ;; init ecx to be 0 , ecx=>counter
  cmp edx, 0    ;;if edx if 0 it means the number is 0
  je endNumber
  
counting:
  mov bl, byte [edx]  ; bl contain the data of the node
  add edx,1		;edx point the the pointer of next node
  mov al,1
countLoop:
  cmp al,0     ;;if al if 0 it because, we are done loocking in the number
  jne c_countLoop
  cmp dword[edx],0   ;if edx points at 0 it means we got to the last node
  je endNumber
  mov edx,[edx]   ;edx cointain the pointer to next node
  jmp counting
c_countLoop:
  mov ah, bl
  and ah, al
  cmp ah,0
  je dontAdd
  inc ecx
dontAdd:
  shl al,1
  jmp countLoop
endNumber:    ;;;; need to fix in case the are more then 16*4 chars
  createNode
  mov dword[ans2],eax
  mov ebx, eax
  mov byte[ebx], cl
makeNumberLoop:
  mov dword [ebx+1],0
  shr ecx, 8
  cmp ecx,0
  je  finishNumber
  createNode
  mov dword [ebx+1],eax
  mov ebx, eax
  mov byte [ebx], cl
  jmp makeNumberLoop
  
  
finishNumber:  
  mov eax, dword[ans2]
  pushN eax
  endFunction
  ret
  
read: 
  push BUFFER
  call gets		;get input 
  add	esp, 4		;Clean up stack after call
  ret
  
checkstack:
  startFunction
  mov ebx, dword [ebp+4]  ;will be added
  mov eax, dword [ebp+8]	;need to be in
  mov ecx, SIZE	
  sub ecx, dword[offest]     ;ecx contain how much free space
  cmp ecx, ebx		;;comp _-->if it bigger error "overflow" message
  jl  CallOverflow 
  mov ecx, dword [offest]
  cmp ecx, eax		;;comp _-->if it smaller error "not enough" message
  jl  CallInsuffcient
  endFunction
  mov eax ,1			;;if every thing is ok return 1
  ret
  
CallInsuffcient:
  call insufficient
  endFunction
  mov eax ,0
  ret
  
CallOverflow:
  call overflow
  endFunction
  mov eax ,0
  ret
  
  
  
  
return:
  
overflow:
  push LC0
  call printf
  add esp, 4 ;clean stack
  ret

insufficient:
  push LC1
  call printf
  add esp, 4 ;clean stack
  ret

  
getNumber:
  mov eax, dword[esp+4]
  mov dword[ans],eax
  startFunction
  mov eax,dword[ans]
  mov ebx,0      ;;the number!!!
  mov ecx,0	;;; the parts of data in the nodes
  cmp eax, 0
  je gotNumber
  mov edx,0
.loop1:
  mov cl, byte[eax]
  push ecx
  inc edx
  cmp dword [eax+1],0
  je .loop2
  mov eax ,dword[eax+1]
  jmp .loop1
.loop2:
  pop ecx
  dec edx
  shl ebx, 8
  mov bl,cl
  cmp edx,0
  je gotNumber
  jmp .loop2
  ;;;;;now edx contain the full y number
gotNumber:
  mov eax,ebx
  endFunctionAns
  ret
  
  
shrFun:
  mov eax, dword[esp+4]
  mov dword[ans],eax
  startFunction
  mov eax,dword[ans]
  cmp eax,0
  je endShrFun
  shr byte [eax],1
shrLoop:
  cmp dword[eax +1],0
  je endShrFun
  mov ebx, [eax+1]
  mov cl ,byte [ebx]
  and cl ,1 
  cmp cl, 0     ;if the lsb of the next node is 1, we need to add 128 to our node
  jne  add128
c_shrFun:
  mov eax, [eax+1]
  shr byte [eax],1
  jmp shrLoop
  
add128:
  add byte [eax],128
  jmp c_shrFun
  

endShrFun:
  endFunction
  ret

cleanZero:
  mov eax, dword[esp+4]
  mov dword[ans],eax
  startFunction
  mov eax,dword[ans]
  cmp eax,0
  je endCleanZero
  cmp dword[eax+1],0
  je endCleanZero
.loop:
  mov ebx,dword[eax+1]
  cmp dword [ebx+1],0
  je cleanAtTheLast
  mov eax,ebx
  jmp .loop
  
cleanAtTheLast:
  cmp byte[ebx],0
  jne endCleanZero
  mov dword[eax+1],0

endCleanZero:
  endFunction
  ret
  
checkExponent:
  mov eax, dword[esp+4]
  mov dword[ans],eax
  startFunction
  mov eax,dword[ans]
  mov ebx,0
  cmp eax,0
  je endCheckExponent
  mov ebx,1
  cmp dword[eax+1],0
  je endCheckExponent
  .loop:
  mov eax,dword[eax+1]
  inc ebx
  cmp dword [eax+1],0
  je endCheckExponent
  jmp .loop
  
endCheckExponent:
  cmp ebx, 4 
  jl ExponentOk
  jmp ExponentNotOk
  
ExponentOk:
  mov eax,1
  endFunctionAns
  ret
ExponentNotOk:
  push LC6
  call printf
  add esp,4
  mov eax,0
  endFunctionAns
  ret
  
  
shlFun:
  mov eax, dword[esp+4]
  mov dword[ans],eax
  startFunction
  mov eax,dword[ans]
  cmp eax,0
  je endShlFun
  mov cl,byte [eax]
  shl cl,1
  PUSHFD
  mov byte [eax],cl
shlLoop:
  cmp dword[eax +1],0
  je endShlFun
  mov eax, [eax+1]
  mov cl,byte [eax]
  POPFD
  rcl cl,1
  PUSHFD
  mov byte[eax],cl
  jmp shlLoop


endShlFun:
  POPFD
  endFunction
  ret
  
  
addZeroBeg:
  mov eax, dword[esp+4]
  mov dword[ans],eax
  startFunction
  mov eax,dword[ans]
  cmp eax,0
  je endaddZero
  mov ebx,eax
.loop:      ;find the end!!!!
  cmp dword[ebx+1],0
  je doZero
  mov ebx,dword[ebx+1]
  jmp .loop
  
doZero:
  createNode
  mov byte[eax], 0
  mov dword[eax+1],0
  mov [ebx+1],eax

endaddZero:
  endFunction
  ret
  
  
  
  
  